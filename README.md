## Fills missing posters of HTML5 video elements

Requires ffmpeg to be usable. Using this in production is probably a terrible
idea.

As a side effect, both video stream and generated poster are cached locally.

## Installation

1. git clone to ``plugins.local/af_video_fill_poster``
2. Enable plugin

## License

GPLv3
