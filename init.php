<?php
class Af_Video_Fill_Poster extends Plugin {

	/* @var PluginHost $host */
	private $host;

	/* @var DiskCache $cache */
	private $cache;

	function about() {
		return array(1.0,
			"Fills missing poster attributes of HTML5 video elements (uses ffmpeg)",
			"fox");
	}

	function init($host) {
		$this->host = $host;
		$this->cache = new DiskCache("images");

		$host->add_hook($host::HOOK_ARTICLE_FILTER, $this, 100);
	}

	private function ffmpeg_thumbnail($input_filename, $output_filename) {
		$cmdline_tpl = "ffmpeg -ss 00:00:01 -i %s -frames:v 1 ".
			"-f singlejpeg %s 2>/dev/null";

		$rc = -1;
		$cmdline = sprintf($cmdline_tpl, escapeshellarg($input_filename), escapeshellarg($output_filename));

		exec($cmdline, $output, $rc);;

		return $rc === 0;
	}

	function hook_article_filter($article) {
		$need_saving = false;
		$doc = new DOMDocument();

		if (@$doc->loadHTML('<?xml encoding="UTF-8">' . $article["content"])) {
			$xpath = new DOMXPath($doc);
			$videos = $xpath->query('(//video)');

			foreach ($videos as $video) {
				if (!$video->hasAttribute("poster")) {
					$source = $xpath->query("//source[@src]", $video)->item(0);

					if ($source) {
						$video_url = $source->getAttribute("src");
						$local_filename = sha1($video_url);
						$thumb_filename = $local_filename . "-POSTER";

						if ($this->cache->exists($thumb_filename)) {
							$video->setAttribute("poster", $this->cache->getUrl($thumb_filename));
							$need_saving = true;

						} else {
							if ($this->cache->exists($local_filename)) {
								if ($this->ffmpeg_thumbnail(
									$this->cache->getFullPath($local_filename),
									$this->cache->getFullPath($thumb_filename))) {

									$video->setAttribute("poster", $this->cache->getUrl($thumb_filename));
									$need_saving = true;
								}
							} else {
								$data = fetch_file_contents(["url" => $video_url, "max_size" => MAX_CACHE_FILE_SIZE]);

								if ($data) {
									if ($this->cache->put($local_filename, $data)) {
										if ($this->ffmpeg_thumbnail(
											$this->cache->getFullPath($local_filename),
											$this->cache->getFullPath($thumb_filename))) {

											$video->setAttribute("poster", $this->cache->getUrl($thumb_filename));
											$need_saving = true;
										}
									}
								}
							}
						}
					}
				}
			}

			if ($need_saving)
				$article["content"] = $doc->saveHTML();
		}

		return $article;
	}

	function api_version() {
		return 2;
	}
}
